
import javax.swing.JOptionPane;

public class Assingment3 
{ 
   public static void main(String[] args){
    
      String correctUsername= "Heiwad";   //Correct Username
      String Username="";                 //User input
      final int LIMIT=3;                  //Limit of logins
      int count=0;                        //counter of logins
      String correctPassword= "password"; //correct password
      String Password= "";                //user input
      String accountType= "Student";      //accounttype
   
      Username = JOptionPane.showInputDialog("Enter Your Username");
      
      while(!Username.equalsIgnoreCase(correctUsername)) //while incorrect username
      {
         count++;                                        //increase number of logins
         if(count == LIMIT)                              //when count reaches the limit lock account and break loop
         {
            JOptionPane.showMessageDialog(null, "Your account is locked. Contact Admin");
            break;
         }
         Username= JOptionPane.showInputDialog("Incorrect Username. Please Enter Again");
      }
      if(count !=LIMIT)                                  //if count does not reach limit ask for password
      {
         Password = JOptionPane.showInputDialog("Enter Your Password");
      
         while(!Password.equalsIgnoreCase(correctPassword)) //while password is incorrect
         {
            count++;                                        //increase number of logins
            if(count == LIMIT)                              //if count reaches limit lock account and break loop
            {
               JOptionPane.showMessageDialog(null, "Your account is locked. Contact Admin");
               break;
            }
            Password= JOptionPane.showInputDialog("Incorrect Password. Please Enter Again");
         }
         if(Password.equalsIgnoreCase(correctPassword) && Username.equalsIgnoreCase(correctUsername)) //if user enters correct username and password welcome them
         {
            JOptionPane.showMessageDialog(null, "Welcome " + Username);
         
            String[] choices = { "Admin", "Student", "Staff"};
            String input = (String) JOptionPane.showInputDialog(null, "Choose account type...","Account Type",JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]);  //display accounttype options
         
            while(input != accountType)                     //while input is not account type ask for new input
            {
               switch(input)
               {
                  case "Admin": 
                     if(accountType == "Admin")
                        break;
                     else
                        input = (String) JOptionPane.showInputDialog(null, "Incorrect account type. Choose account type...","Account Type",JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]);  
                                               
                  case "Student":
                     if(accountType == "Student")
                        break;
                     else
                        input = (String) JOptionPane.showInputDialog(null, "Incorrect account type. Choose account type...","Account Type",JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]);  
                  case "Staff":
                     if(accountType == "Staff")
                        break;
                     else
                        input = (String) JOptionPane.showInputDialog(null, "Incorrect account type. Choose account type...","Account Type",JOptionPane.QUESTION_MESSAGE, null,choices,choices[1]);  
               }
            }
            JOptionPane.showMessageDialog(null, "Welcome " +input);
         }
      }
   }
}